package org.example.asteroides;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Vector;

/**
 * Created by pauchorroyanguas on 9/9/15.
 */
public class AdaptadorPuntuaciones extends BaseAdapter {

    private final Activity actividad;
    private final Vector<String> lista;


    public AdaptadorPuntuaciones(Activity actividad, Vector<String> lista)
    {
        super();
        this.actividad = actividad;
        this.lista = lista;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = actividad.getLayoutInflater();
        View v = inflater.inflate(R.layout.elementolista_puntuaciones, null, true);

        TextView tvTitulo = (TextView) v.findViewById(R.id.titulo);
        tvTitulo.setText(lista.elementAt(position));
        ImageView ivIcono = (ImageView) v.findViewById(R.id.icono);

        switch(Math.round((float)Math.random()*3))
        {
            case 0:
                ivIcono.setImageResource(R.drawable.asteroide1);
                break;
            case 1:
                ivIcono.setImageResource(R.drawable.asteroide2);
                break;
            default:
                ivIcono.setImageResource(R.drawable.asteroide3);
                break;
        }

        return v;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.elementAt(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}

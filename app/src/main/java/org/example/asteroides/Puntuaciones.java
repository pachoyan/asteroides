package org.example.asteroides;

import android.app.ListActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.example.asteroides.fuentedatos.AlmacenPuntuaciones;
import org.example.asteroides.fuentedatos.AlmacenPuntuacionesArray;
import org.example.asteroides.fuentedatos.FactoriaDAOS;


public class Puntuaciones extends ListActivity {

    AlmacenPuntuaciones fuenteDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puntuaciones);

        fuenteDatos = FactoriaDAOS.getInstance(this);

        if(fuenteDatos.listaPuntuaciones(10) !=null)
            setListAdapter(new AdaptadorPuntuaciones(this, fuenteDatos.listaPuntuaciones(10)));

        //setListAdapter(new ArrayAdapter<String>(this,
        //        R.layout.elementolista_puntuaciones, R.id.titulo, fuenteDatos.listaPuntuaciones(10)));
    }

    @Override protected void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        Object o = getListAdapter().getItem(position);
        int posicion = position + 1;
        Toast.makeText(this, "Selección: " + posicion
                + " - " + o.toString(), Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_puntuaciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

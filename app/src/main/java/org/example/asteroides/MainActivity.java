package org.example.asteroides;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.media.MediaPlayer;
import android.os.StrictMode;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.example.asteroides.fuentedatos.AlmacenPuntuaciones;
import org.example.asteroides.fuentedatos.FactoriaDAOS;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements GestureOverlayView.OnGesturePerformedListener {

    AlmacenPuntuaciones fuenteDatos;

    MediaPlayer mp;

    private GestureLibrary libreria;

    Button bJugar;
    Button bPuntuaciones;
    Button bConfiguracion;
    Button bAcercade;

    TextView tvTitulo;

    //Animaciones
    Animation animacionGiroZoom;
    Animation animacionAparecer;
    Animation animacionDesplazamiento;
    Animation animacionDesplazamientoRetorno;
    Animation animacionArribaAbajo;

    GestureOverlayView gesturesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

        //TODO Activar servicio musica
        //startService(new Intent(MainActivity.this, ServicioMusica.class));

        Log.e("TAG_MAIN_ACTIVITY", "OnCreate: Obtengo nueva instancia...");
        fuenteDatos = FactoriaDAOS.getInstance(this);

        //TODO activar musica
        //Music
        //mp = MediaPlayer.create(this, R.raw.audio);
        //mp.start();

        bJugar = (Button) findViewById(R.id.Button01);
        bConfiguracion = (Button) findViewById(R.id.Button02);
        bAcercade = (Button) findViewById(R.id.Button03);
        bPuntuaciones = (Button) findViewById(R.id.Button04);
        tvTitulo = (TextView) findViewById(R.id.titulo);

        //TODO descomentar animaciones
        /*animacionGiroZoom = AnimationUtils.loadAnimation(this, R.anim.giro_con_zoom);
        animacionAparecer = AnimationUtils.loadAnimation(this, R.anim.aparecer);
        animacionDesplazamiento = AnimationUtils.loadAnimation(this, R.anim.desplazamiento_derecha);
        animacionDesplazamientoRetorno = AnimationUtils.loadAnimation(this, R.anim.desplazamiento_con_retorno);
        animacionArribaAbajo = AnimationUtils.loadAnimation(this, R.anim.arriba_abajo);

        tvTitulo.startAnimation(animacionGiroZoom);
        bJugar.startAnimation(animacionAparecer);
        bConfiguracion.startAnimation(animacionDesplazamiento);
        bAcercade.startAnimation(animacionDesplazamientoRetorno);
        bPuntuaciones.startAnimation(animacionArribaAbajo);*/

        bJugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarPreferencias(null);
                lanzarJuego(null);

            }
        });

        bConfiguracion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                lanzaraPreferencias(null);
            }
        });

        bAcercade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lanzaracercade(null);

                //TODO Activar animacion
                //Le aplico la animacion al mismo boton :)
                //bAcercade.startAnimation(animacionGiroZoom);
            }
        });



        bPuntuaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lanzaraPuntuaciones(null);
                //finish();

            }
        });


        //Gesture
        libreria = GestureLibraries.fromRawResource(this, R.raw.gestures);

        if (!libreria.load()) {
            finish();
        }


        gesturesView =
                (GestureOverlayView) findViewById(R.id.gestures);
        gesturesView.addOnGesturePerformedListener(this);


    }

    /**************
     Ciclo de vida
     **************/
    @Override protected void onStart() {
        super.onStart();

    }
    @Override protected void onPause() {
        super.onPause();

        //Comentado en el 1, ahora lo dejo así para el 2:)
        //if(mp.isPlaying())
           // mp.pause();
    }
    @Override protected void onResume() {
        super.onResume();
        Log.e("TAG_MAIN_ACTIVITY", "onResume(): Obtengo nueva instancia...");
        fuenteDatos = FactoriaDAOS.getInstance(this);
       // if (!mp.isPlaying())
           // mp.start();
    }
    @Override protected void onStop() {
        super.onStop();

       /* if (mp.isPlaying())
            mp.stop();*/
    }

    @Override protected void onRestart() {
        super.onRestart();

    }
    @Override protected void onDestroy() {
        super.onDestroy();
        //TODO Desactivar serviciomusica
        stopService(new Intent(MainActivity.this, ServicioMusica.class));

       /* if(mp.isPlaying())
            mp.stop();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //No lo ponemos...
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            lanzaraPreferencias(null);
            return true;
        }

        if (id == R.id. acercaDe) {
            lanzaracercade(null);
            return true;
        }

        //Es el que esta
        if (id == R.id.config)
        {
            lanzaraPreferencias(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void lanzaracercade(View view)
    {
        Intent i = new Intent(this, Acercade.class);
        startActivity(i);
    }

    public void lanzaraPreferencias(View view)
    {
        Intent i = new Intent(this, Preferencias.class);
        startActivity(i);
    }

    public void lanzaraPuntuaciones(View view)
    {
        Log.e("TAG_MAIN_ACTIVITY", "lanzarPuntuaciones(): Lanzo las puntuaciones...");
        Intent i = new Intent(this, Puntuaciones.class);
        startActivity(i);
    }

   /* Deprecated
    public void lanzarjuego(View view)
    {
        Intent i = new Intent(this, Juego.class);
        startActivity(i);
    }*/

    public void lanzarJuego(View view) {
        Intent i = new Intent(this, Juego.class);
        startActivityForResult(i, 1234);
    }

    @Override protected void onActivityResult (int requestCode,
                                               int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1234 && resultCode==RESULT_OK && data!=null) {
            int puntuacion = data.getExtras().getInt("puntuacion");
            String nombre = "Yo";
            // Mejor leer nombre desde un AlertDialog.Builder o preferencias
            Log.e("TAG_MAIN_ACTIVITY", "onActivityResult(): Guardo las puntuaciones...");
            fuenteDatos.guardarPuntuacion(puntuacion, nombre, System.currentTimeMillis());

            lanzaraPuntuaciones(null);
        }
    }

    public void mostrarPreferencias(View view){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String s = "música: " + pref.getBoolean("musica",true)
                +", gráficos: " + pref.getString("graficos","?") +
                ", fragmentos: " + pref.getString("fragmentos", "?") +
                ", multijugador: " + pref.getBoolean("activarmultijugador" , true) +
                ", maxjugadores: " + pref.getString("maxjugadores", "?") +
                ", conexion: " + pref.getString("conexion", "?");

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    /**
     * Gestures
     * @param overlay
     * @param gesture
     */
    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {

        ArrayList<Prediction> predictions=libreria.recognize(gesture);
        if (predictions.size()>0) {
            String comando = predictions.get(0).name;

            if (comando.equals("play")){
                lanzarJuego(null);
            } else if (comando.equals("configurar")){
                lanzaraPreferencias(null);
            } else if (comando.equals("acerca_de")){
                lanzaracercade(null);
            } else if (comando.equals("cancelar")){
                finish();
            }
        }

    }

    /**
     * Estado actividad
     * @param estadoGuardado
     */
    @Override
    protected void onSaveInstanceState(Bundle estadoGuardado){
        super.onSaveInstanceState(estadoGuardado);
        if (mp != null) {
            int pos = mp.getCurrentPosition();
            estadoGuardado.putInt("posicion", pos);
        }
    }
    @Override
    protected void onRestoreInstanceState(Bundle estadoGuardado){
        super.onRestoreInstanceState(estadoGuardado);
        if (estadoGuardado != null && mp != null) {
            int pos = estadoGuardado.getInt("posicion");
            mp.seekTo(pos);
        }
    }

    /*public void salir(View view)
    {
        finish();
    }*/

}



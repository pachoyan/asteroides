package org.example.asteroides.Vistas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import java.util.Vector;
//Import :(
import org.example.asteroides.R;


/**
 * Created by pauchorroyanguas on 10/9/15.
 */
public class VistaJuego extends View implements SensorEventListener {

    SensorManager mSensorManager;

    private Context context;

    private Drawable drawableAsteroide[] = new Drawable[3];

    //Puntuaciones
    private int puntuacion = 0;
    private Activity padre;

    //CONST
    private static final int TIPO_ACELEROMETRO = 0;
    private static final int TIPO_ORIENTACION = 1;
    private static final int TIPO_TACTIL = 2;
    private static final int TIPO_TECLADO = 3;
    private int TIPO = 0;

    //Sensores
    private boolean hayValorInicial = false;
    private float valorInicial;
    private float valorInicialZ;

    //Nave
    private float mX=0, mY=0; //Recordar las coordenadas del ultimo evento
    private boolean disparo=false;


    // //// MULTIMEDIA //////
    SoundPool soundPool;
    int idDisparo, idExplosion;

    // //// MISIL //////
    //private Grafico misil;
    //private int numeroMisiles = 0;
    private int numMisiles = 0;
    private Vector<Grafico> misiles;
    private static int PASO_VELOCIDAD_MISIL = 12;
    //private boolean misilActivo = false;
    private int tiempoMisil;
    private Vector<Integer> tiempoMisiles;
    Drawable drawableMisil;

    // //// THREAD Y TIEMPO //////
    // Thread encargado de procesar el juego private
    ThreadJuego thread = new ThreadJuego();
    // Cada cuanto queremos procesar cambios (ms)
    private static int PERIODO_PROCESO = 50;
    // Cuando se realizó el último proceso
    private long ultimoProceso = 0;

    // //// ASTEROIDES //////
    private Vector<Grafico> asteroides; // Vector con los Asteroides
    private int numAsteroides = 3/*5*/; // Número inicial de asteroides
    private int numFragmentos = 3; // Fragmentos en que se divide

    // //// NAVE //////
    private Grafico nave; // Gráfico de la nave
    private int giroNave; // Incremento de dirección
    private double aceleracionNave; // aumento de velocidad
    private static final int MAX_VELOCIDAD_NAVE = 20;
    // Incremento estándar de giro y aceleración
    private static final int PASO_GIRO_NAVE = 5;
    private static final float PASO_ACELERACION_NAVE = 0.5f;

    public VistaJuego(Context context, AttributeSet attrs) {
        super(context, attrs);
        Drawable drawableNave;


        soundPool = new SoundPool( 5, AudioManager.STREAM_MUSIC , 0);
        idDisparo = soundPool.load(context, R.raw.disparo, 0);
        idExplosion = soundPool.load(context, R.raw.explosion, 0);


        this.context = context;
        //ASTEROIDES
        /*drawableAsteroide = context.getResources().getDrawable(
                R.drawable.asteroide1);*/

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());

        if (pref.getString("graficos", "1").equals("0")) {

            //NAVE
            Path pathNave = new Path();
            pathNave.moveTo(0.0f, 0.0f);
            pathNave.lineTo(1.0f, 0.5f);
            pathNave.lineTo(0.0f, 1.0f);
            pathNave.lineTo(0.0f, 0.0f);

            ShapeDrawable dNave = new ShapeDrawable(new PathShape(pathNave, 1, 1));
            dNave.getPaint().setColor(Color.WHITE);
            dNave.getPaint().setStyle(Paint.Style.STROKE);
            dNave.setIntrinsicHeight(15);
            dNave.setIntrinsicWidth(20);
            drawableNave = dNave;

            //ASTEROIDES
            Path pathAsteroide = new Path();
            pathAsteroide.moveTo((float) 0.3, (float) 0.0);
            pathAsteroide.lineTo((float) 0.6, (float) 0.0);
            pathAsteroide.lineTo((float) 0.6, (float) 0.3);
            pathAsteroide.lineTo((float) 0.8, (float) 0.2);
            pathAsteroide.lineTo((float) 1.0, (float) 0.4);
            pathAsteroide.lineTo((float) 0.8, (float) 0.6);
            pathAsteroide.lineTo((float) 0.9, (float) 0.9);
            pathAsteroide.lineTo((float) 0.8, (float) 1.0);
            pathAsteroide.lineTo((float) 0.4, (float) 1.0);
            pathAsteroide.lineTo((float) 0.0, (float) 0.6);
            pathAsteroide.lineTo((float) 0.0, (float) 0.2);
            pathAsteroide.lineTo((float) 0.3, (float) 0.0);

            /*ShapeDrawable dAsteroide = new ShapeDrawable(new PathShape(pathAsteroide, 1, 1));
            dAsteroide.getPaint().setColor(Color.WHITE);
            dAsteroide.getPaint().setStyle(Paint.Style.STROKE);
            dAsteroide.setIntrinsicWidth(50);
            dAsteroide.setIntrinsicHeight(50);*/
            //drawableAsteroide = dAsteroide;

            for (int i=0; i<3; i++) {
                ShapeDrawable dAsteroide = new ShapeDrawable(new PathShape(
                        pathAsteroide, 1, 1)); dAsteroide.getPaint().setColor(Color.WHITE);
                dAsteroide.getPaint().setStyle(Paint.Style.STROKE);
                dAsteroide.setIntrinsicWidth(50 - i * 14);
                dAsteroide.setIntrinsicHeight(50 - i * 14);
                drawableAsteroide[i] = dAsteroide;
            }


            setBackgroundColor(Color.BLACK);

            ShapeDrawable dMisil = new ShapeDrawable(new RectShape());
            dMisil.getPaint().setColor(Color.WHITE);
            dMisil.getPaint().setStyle(Paint.Style.STROKE);
            dMisil.setIntrinsicWidth(15);
            dMisil.setIntrinsicHeight(3);
            drawableMisil = dMisil;

            //API MIN 11
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);



        } else {

            drawableNave = context.getResources().getDrawable(R.drawable.nave);
            //drawableAsteroide = context.getResources().getDrawable(
            // R.drawable.asteroide1);

            drawableAsteroide[0] = context.getResources(). getDrawable(R.drawable.asteroide1);
            drawableAsteroide[1] = context.getResources(). getDrawable(R.drawable.asteroide2);
            drawableAsteroide[2] = context.getResources(). getDrawable(R.drawable.asteroide3);
            drawableMisil = context.getResources().getDrawable(R.drawable.misil1);

            //API MIN 11
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                setLayerType(View.LAYER_TYPE_HARDWARE, null);

        }

        //NAVE
        nave = new Grafico(this, drawableNave);

        asteroides = new Vector<Grafico>();
        for (int i = 0; i < 3/*numAsteroides*/; i++) {
            Grafico asteroide = new Grafico(this, drawableAsteroide[i]);
            asteroide.setIncY(Math.random() * 4 - 2);
            asteroide.setIncX(Math.random() * 4 - 2);
            asteroide.setAngulo((int) (Math.random() * 360));
            asteroide.setRotacion((int) (Math.random() * 8 - 4));
            asteroides.add(asteroide);


        }

        //misil = new Grafico(this, drawableMisil);
        misiles = new Vector<Grafico>();
        /*for(int i = 0; i < numMisiles; i++)
        {
            Grafico misil = new Grafico(this, drawableMisil);
            misiles.add(misil);
        }*/
        tiempoMisiles = new Vector<Integer>();
        /*for(int i = 0; i < numMisiles; i++)
        {
            tiempoMisiles.add(PASO_VELOCIDAD_MISIL);
        }*/

        //Sensores

        TIPO = obtenerPreferencias();

        /*Activar sensores

        mSensorManager = (SensorManager) getContexto().getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensors = mSensorManager.getSensorList(
                Sensor.TYPE_ORIENTATION);
        if (!listSensors.isEmpty()) {
            Sensor orientationSensor = listSensors.get(0);
            mSensorManager.registerListener(this, orientationSensor,
                    SensorManager.SENSOR_DELAY_GAME);
        }

        listSensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        if(!listSensors.isEmpty())
        {
            Sensor acelerometroSensor = listSensors.get(0);
            mSensorManager.registerListener(this, acelerometroSensor, SensorManager.SENSOR_DELAY_GAME);
        }
        * */


    }

    @Override
    protected void onSizeChanged(int ancho, int alto,
                                 int ancho_anter, int alto_anter) {
        super.onSizeChanged(ancho, alto, ancho_anter, alto_anter);
        // Una vez que conocemos nuestro ancho y alto.

        //NAVE
        nave.setCenX(ancho / 2);
        nave.setCenY(alto / 2);

        //ASTEROIDES
        for (Grafico asteroide : asteroides) {
            do {
                asteroide.setCenX((int) (Math.random()*ancho));
                asteroide.setCenY((int) (Math.random()*alto));
            } while(asteroide.distancia(nave) < (ancho+alto)/5);
        }

        ultimoProceso = System.currentTimeMillis();
        //Thread fisica
        thread.start();

    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //NAVE
        nave.dibujaGrafico(canvas);

        //ASTEROIDES
        for (Grafico asteroide : asteroides) {
            asteroide.dibujaGrafico(canvas);
        }

        //MISIL
        /*if(misilActivo)
        {
            misil.dibujaGrafico(canvas);
        }*/
        if(!misiles.isEmpty())
        {
            for (Grafico misil : misiles) {
                misil.dibujaGrafico(canvas);
            }
        }

    }

    //Actualiza la fisica
    protected synchronized void actualizaFisica() {
        long ahora = System.currentTimeMillis();
        if (ultimoProceso + PERIODO_PROCESO > ahora) {
            return; // Salir si el período de proceso no se ha cumplido.
        }

        // Para una ejecución en tiempo real calculamos retardo
        double retardo = (ahora - ultimoProceso) / PERIODO_PROCESO;
        ultimoProceso = ahora; // Para la próxima vez

        // Actualizamos velocidad y dirección de la nave a partir de
        // giroNave y aceleracionNave (según la entrada del jugador)
        nave.setAngulo((int) (nave.getAngulo() + giroNave * retardo));
        double nIncX = nave.getIncX() + aceleracionNave * Math.cos(Math.toRadians(nave.getAngulo())) * retardo;
        double nIncY = nave.getIncY() + aceleracionNave * Math.sin(Math.toRadians(nave.getAngulo())) * retardo;

        // Actualizamos si el módulo de la velocidad no excede el máximo
        if (Math.hypot(nIncX,nIncY) <= MAX_VELOCIDAD_NAVE){ nave.setIncX(nIncX);
            nave.setIncY(nIncY);
        }
        nave.incrementaPos(retardo); // Actualizamos posición
        for (Grafico asteroide : asteroides) {
            asteroide.incrementaPos(retardo);
        }

        // Actualizamos posición de misil
        /*if (misilActivo) {
        misil.incrementaPos(retardo);
        tiempoMisil-=retardo;
        if (tiempoMisil < 0) {
            misilActivo = false;
        } else {
            for (int i = 0; i < asteroides.size(); i++)
            {
                if (misil.verificaColision(asteroides.elementAt(i))) {
                    destruyeAsteroide(i);
                    break;
                }
            }
        }*/

        for(int i = 0; i < misiles.size(); i++)
        {

            misiles.get(i).incrementaPos(retardo);
            tiempoMisil = tiempoMisiles.get(i);
            tiempoMisil-=retardo;
            tiempoMisiles.setElementAt(tiempoMisil, i);
            if (tiempoMisiles.get(i) < 0) {
                //misilActivo = false;
                misiles.remove(i);
                tiempoMisiles.remove(i);
                numMisiles--;

            } else {
                for (int j = 0; j < asteroides.size(); j++)
                {
                    if (misiles.get(i).verificaColision(asteroides.elementAt(j))) {
                        destruyeAsteroide(j);
                        //Destruye Misil
                        //misiles.remove(i);
                        break;
                    }
                }
            }
        }

        for (Grafico asteroide : asteroides) {
            if (asteroide.verificaColision(nave)) {
                salir(); }
        }
    }

    private void destruyeAsteroide(int i) {

        int tam;
        if(asteroides.get(i).getDrawable()!=drawableAsteroide[2]) {
            if (asteroides.get(i).getDrawable() == drawableAsteroide[1]) {
                tam = 2;
            } else {
                tam = 1;
            }


            for (int n = 0; n < numFragmentos; n++) {
                Grafico asteroide = new Grafico(this, drawableAsteroide[tam]);
                asteroide.setCenX(asteroides.get(i).getCenX());
                asteroide.setCenY(asteroides.get(i).getCenY());
                asteroide.setIncX(Math.random() * 7 - 2 - tam);
                asteroide.setIncY(Math.random() * 7 - 2 - tam);
                asteroide.setAngulo((int) (Math.random() * 360));
                asteroide.setRotacion((int) (Math.random() * 8 - 4));
                asteroides.add(asteroide);
            }
        }

        asteroides.remove(i);
        puntuacion += 100;
        soundPool.play(idExplosion, 1, 1, 0, 0, 1);

        if(asteroides.isEmpty()){
            salir();
        }
        //misilActivo = false;
    }

    private void activaMisil(int pos) {


        misiles.add(new Grafico(this, drawableMisil));
        tiempoMisiles.add(PASO_VELOCIDAD_MISIL);

        misiles.get(pos).setCenX(nave.getCenX());
        misiles.get(pos).setCenY(nave.getCenY());
        misiles.get(pos).setAngulo(nave.getAngulo());
        misiles.get(pos).setIncX(Math.cos(Math.toRadians(misiles.get(pos).getAngulo())) *
                PASO_VELOCIDAD_MISIL);
        misiles.get(pos).setIncY(Math.sin(Math.toRadians(misiles.get(pos).getAngulo())) *
                PASO_VELOCIDAD_MISIL);
        tiempoMisiles.setElementAt((int) Math.min(this.getWidth() / Math.abs( misiles.get(pos).
                getIncX()), this.getHeight() / Math.abs(misiles.get(pos).getIncY())) - 2, pos);

        soundPool.play(idDisparo, 1, 1, 1, 0, 1.5f);
        //misilActivo = true;
    }


    /**
     * Thread game
     */
    public class ThreadJuego extends Thread {

        private boolean pausa, corriendo;

        public synchronized void pausar() {
            pausa = true;
        }

        public synchronized void reanudar() {
            pausa = false;
            notify();
        }

        public void detener() {
            corriendo = false;
            if (pausa) reanudar();
        }

        @Override
        public void run() {
            corriendo = true;
            while (corriendo) {
                actualizaFisica();
                synchronized (this) {
                    while (pausa) {
                        try {
                            wait();
                        } catch (Exception e) {
                        }
                    }
                }
            }

        }
    }

    public ThreadJuego getThread() {
        return thread;
    }

    /**
     * Manejo nave con pantalla tactil
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent (MotionEvent event) {
        super.onTouchEvent(event);
            //Este tipo de manejo no lo desactivaremos nunca ya que no molesta,....

            //Log.i("TIPO TACTIL: ",  "Sensor tipo tactil");
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    disparo = true;
                    break;
                case MotionEvent.ACTION_MOVE:
                    float dx = Math.abs(x - mX);
                    float dy = Math.abs(y - mY);

                    if (dy < 6 && dx > 6) {
                        giroNave = Math.round((x - mX) / 2);
                        disparo = false;
                    } else if (dx < 6 && dy > 6) {
                        if (mY - y > 0) {
                            aceleracionNave = Math.round((mY - y) / 25);
                            disparo = false;
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    giroNave = 0;
                    aceleracionNave = 0;
                    if (disparo) {
                        activaMisil(numMisiles);
                        numMisiles++;
                    }
                    break;
            }
            mX = x;
            mY = y;
            return true;

    }



    /**
     * Metodo manejo nave con teclado
     * @param codigoTecla
     * @param evento
     * @return
     */
    @Override
    public boolean onKeyDown(int codigoTecla, KeyEvent evento) {
        super.onKeyDown(codigoTecla, evento);

        if(TIPO == TIPO_TECLADO) {
            //Log.i("TIPO TECLADO: ",  "Sensor tipo teclado");
            boolean procesada = true;
            switch (codigoTecla) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    aceleracionNave = +PASO_ACELERACION_NAVE;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    giroNave = -PASO_GIRO_NAVE;
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    giroNave = +PASO_GIRO_NAVE;
                    break;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    //ActivaMisil();
                    break;
                default:
                    procesada = false;
                    break;
            }
            return procesada;
        }
        else
        {
            return false;
        }
    }

    /**
     * Sensores Orientacion y Acelerometro
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        switch(TIPO) {
            case TIPO_ACELEROMETRO:
                //Log.i("TIPO ACELEROMETRO: ",  "Sensor tipo acelerometro");
                float valorY = event.values[1];
                float valorZ = event.values[2];
                if(!hayValorInicial)
                {
                    valorInicial = valorY;
                    valorInicialZ = valorZ;
                    hayValorInicial = true;
                }
                giroNave=(int) (valorY-valorInicial)/4;
                aceleracionNave = (int) (valorZ - valorInicialZ)/2;

                break;
            case TIPO_ORIENTACION:
                //Log.i("TIPO ORIENTACION: ",  "Sensor tipo orientacion");
                float valor = event.values[1];
                if (!hayValorInicial){
                    valorInicial = valor;
                    hayValorInicial = true;
                }
                giroNave=(int) (valor-valorInicial)/3 ;
                break;

        }
    }

    /**
     * Sensores Orientacion y Acelerometro
     * @param sensor
     * @param accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {



    }


    public int obtenerPreferencias(){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());

        /*0.Acelerometro
          1.Orientacion
          2.Tactil
          3.Teclado*/
        String sensorElegido = pref.getString("sensor", "?");

        if(sensorElegido.equals("0"))
        {
            return TIPO_ACELEROMETRO;
        }
        else if(sensorElegido.equals("1"))
        {
            return TIPO_ORIENTACION;
        }
        else if(sensorElegido.equals("2"))
        {
            return TIPO_TACTIL;
        }
        else if(sensorElegido.equals("3"))
        {
            return TIPO_TECLADO;
        }
        else
        {
            return TIPO_ACELEROMETRO;
        }


    }
    /***********************************
     * Conectar y desconectar sensores *
     ***********************************/


    public void activarSensores(){

        mSensorManager = (SensorManager) getContexto().getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensors = mSensorManager.getSensorList(
                Sensor.TYPE_ORIENTATION);
        if (!listSensors.isEmpty()) {
            Sensor orientationSensor = listSensors.get(0);
            mSensorManager.registerListener(this, orientationSensor,
                    SensorManager.SENSOR_DELAY_GAME);
        }

        listSensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        if(!listSensors.isEmpty())
        {
            Sensor acelerometroSensor = listSensors.get(0);
            mSensorManager.registerListener(this, acelerometroSensor, SensorManager.SENSOR_DELAY_GAME);
        }

    }

    public void desactivarSensores(){

        mSensorManager.unregisterListener(this);

    }


    public Context getContexto() {
        return context;
    }

    public void setPadre(Activity padre){
        this.padre = padre;
    }

    private void salir() {
        Bundle bundle = new Bundle();
        bundle.putInt("puntuacion", puntuacion);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        padre.setResult(Activity.RESULT_OK, intent);
        padre.finish();
    }
}

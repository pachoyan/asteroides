package org.example.asteroides.fuentedatos;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Vector;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by pauchorroyanguas on 29/10/15.
 */
public class AlmacenPuntuacionesSW_PHP_AsyncTask implements AlmacenPuntuaciones{


    private Context contexto;

    public AlmacenPuntuacionesSW_PHP_AsyncTask(Context contexto) {
        this.contexto = contexto;
    }

    public Vector<String> listaPuntuaciones(int cantidad) {

        try {
            TareaLista tarea = new TareaLista();
            tarea.execute(cantidad);
            return tarea.get(4, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            Toast.makeText(contexto, "Tiempo excedido al conectar",
                    Toast.LENGTH_LONG).show();
        }
        catch (CancellationException e) {
            Toast.makeText(contexto, "Error al conectar con servidor", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(contexto, "Error con tarea asíncrona",
                    Toast.LENGTH_LONG).show();

        }
        return new Vector<String>();
    }


    public void guardarPuntuacion(int puntos, String nombre, long fecha){

        try {
            TareaGuardar tarea = new TareaGuardar(); tarea.execute(String.valueOf(puntos), nombre,
                    String.valueOf(fecha));
            tarea.get(4, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            Toast.makeText(contexto, "Tiempo excedido al conectar",
                    Toast.LENGTH_LONG).show(); } catch (CancellationException e) {
            Toast.makeText(contexto, "Error al conectar con servidor", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(contexto, "Error con tarea asíncrona",Toast.LENGTH_LONG).show();
        }
    }

    private class TareaGuardar extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... param) {


            //Copia el código que antes estaba en guardarPuntuaciones

            try {
                URL url = new URL(
                        "http://www.pauchorropuntuaciones.esy.es/puntuaciones/nueva.php"
                                + "?puntos=" + param[0] + "&nombre="
                                + URLEncoder.encode(param[1], "UTF-8")
                                + "&fecha=" + param[2]);

                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();

                if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    BufferedReader reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    String linea = reader.readLine();
                    if (!linea.equals("OK")) {
                        Log.e("Asteroides", "Error en servicio Web nueva");
                    }
                } else {
                    Log.e("Asteroides", conexion.getResponseMessage());
                }

                conexion.disconnect();
            } catch (Exception e) {
                Log.e("Asteroides", e.getMessage(), e);
            }
            return null;
        }
    }



     class TareaLista extends AsyncTask<Integer, Void, Vector<String>> {

        @Override
        protected Vector<String> doInBackground(Integer... cantidad){
        //Copia el código que antes estaba en listaPuntuaciones()
            Vector<String> result = new Vector<String>();
            HttpURLConnection conexion = null;

            try {

                URL url = new URL("http://www.pauchorropuntuaciones.esy.es/puntuaciones/lista.php"
                        + "?max=20");

                conexion = (HttpURLConnection) url.openConnection();

                if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    String linea = reader.readLine();
                    while (!linea.equals("")) {
                        result.add(linea);
                        linea = reader.readLine();
                    }
                    reader.close();
                    conexion.disconnect();
                    return result;
                } else {

                    Log.e("Asteroides", conexion.getResponseMessage());
                    conexion.disconnect();
                    return result;
                }
            } catch (Exception e) {
                Log.e("Asteroides", e.getMessage(), e);
                conexion.disconnect();
                return result;
            }
        }
    }
}

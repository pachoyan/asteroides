package org.example.asteroides.fuentedatos;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by pauchorroyanguas on 24/10/15.
 */
public class AlmacenPuntuacionesFicheroExternoApl implements  AlmacenPuntuaciones {

    private static String FICHERO = Environment.getExternalStorageDirectory() + "/puntuaciones.txt";
    private Context context;

    public AlmacenPuntuacionesFicheroExternoApl(Context context) {
        this.context = context;
    }

    @Override
    public void guardarPuntuacion(int puntos, String nombre, long fecha) {

        String stadoSD = Environment.getExternalStorageState();
        if (!stadoSD.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(context, "No puedo escribir en la memoria externa",
                    Toast.LENGTH_LONG).show();
            return;
        }

        Log.e("TAG_GUARDAR_PUNTUACIONES", "FICHERO puntuaciones.txt creandose.....");
        try {

            File ruta = new File(Environment.getExternalStorageDirectory() +
                    "/Android/data/org.example.asteroides/files/");
            if (!ruta.exists()) {
                ruta.mkdirs();
            }
            final File fi = new File(ruta, "puntuaciones.txt");
            FileOutputStream f = new FileOutputStream(fi, true);
            String texto = puntos + " " + nombre + "\n";
            f.write(texto.getBytes());
            Log.e("TAG_GUARDAR_PUNTUACIONES", "FICHERO puntuaciones.txt creandose.....");
            f.close();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e); }
    }

    @Override
    public Vector<String> listaPuntuaciones(int cantidad) {

        Vector<String> result = new Vector<String>();

        String stadoSD = Environment.getExternalStorageState();
        if (!stadoSD.equals(Environment.MEDIA_MOUNTED) &&
                !stadoSD.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            Toast.makeText(context, "No puedo leer en la memoria externa",
                Toast.LENGTH_LONG).show();
            return result;
        }
        
        try {
            File ruta = new File(Environment.getExternalStorageDirectory() +
                    "/Android/data/org.example.asteroides/files/");
            if (!ruta.exists()) {
                ruta.mkdirs();
            }
            final File fi = new File(ruta, "puntuaciones.txt");

            FileInputStream f = new FileInputStream(fi);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(f));
            int n = 0;
            String linea;
            do {
                linea = entrada.readLine();
                if (linea != null) { result.add(linea);
                    n++; }
            } while (n < cantidad && linea != null);
            f.close();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
        }

        return result;
    }
}

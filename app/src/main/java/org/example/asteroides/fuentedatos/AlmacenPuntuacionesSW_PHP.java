package org.example.asteroides.fuentedatos;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Vector;

/**
 * Created by pauchorroyanguas on 29/10/15.
 */
public class AlmacenPuntuacionesSW_PHP implements AlmacenPuntuaciones {

    public AlmacenPuntuacionesSW_PHP(Context context)
    {

    }

    public Vector<String> listaPuntuaciones(int cantidad) {

        Vector<String> result = new Vector<String>();
        HttpURLConnection conexion = null;

        try {

            URL url = new URL("http://158.42.146.127/puntuaciones/lista.php"
                    + "?max=20");

            conexion = (HttpURLConnection) url.openConnection();

            if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                String linea = reader.readLine();
                while (!linea.equals("")) {
                    result.add(linea);
                    linea = reader.readLine();
                }
                reader.close();
                conexion.disconnect();
                return result;
            } else {

                Log.e("Asteroides", conexion.getResponseMessage());
                conexion.disconnect();
                return result;
            }
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
            conexion.disconnect();
            return result;
        }
    }


    public void guardarPuntuacion(int puntos, String nombre, long fecha) {
        try {
            URL url = new URL("http://158.42.146.127/puntuaciones/nueva.php"
                    + "?puntos=" + puntos
                    + "&nombre=" + URLEncoder.encode(nombre, "UTF-8") + "&fecha=" + fecha);

            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();

            if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                String linea = reader.readLine();
                if (!linea.equals("OK")) {
                    Log.e("Asteroides", "Error en servicio Web nueva");
                }
            } else {
                Log.e("Asteroides", conexion.getResponseMessage());
            }

            conexion.disconnect();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
        }
    }


}

package org.example.asteroides.fuentedatos;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by pauchorroyanguas on 9/9/15.
 */
public class FactoriaDAOS {

    private static AlmacenPuntuaciones fuenteDatos;

    //private static final String DATABASE_NAME = "puntuaciones";

    //ALMACENES
    private static final String ARRAY = "0";
    private static final String PREFERENCIAS = "1";
    private static final String FICHERO_MEMORIA_INTERNA = "2";
    private static final String FICHERO_MEMORIA_EXTERNA = "3";
    private static final String FICHERO_MEMORIA_EXTERNA_APLICACION = "4";
    private static final String LEER_FICHERO_RAW = "5";
    private static final String LEER_FICHERO_ASSETS = "6";
    private static final String LEER_XML_SAX = "7";
    private static final String LEER_XML_DOM = "8";
    private static final String LEER_SQLITE = "9";
    private static final String LEER_SQLITE_REL = "10";
    private static final String LEER_PROVIDER = "11";
    private static final String SOCKETS = "12";
    private static final String SW_PHP = "13";
    private static final String SW_PHP_ASYNCTASK = "14";
    private static final String SER_WEB = "15";

    public static AlmacenPuntuaciones getInstance(Context context)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String preferenciasPuntaciones = pref.getString("puntuaciones", "0");

        if(preferenciasPuntaciones.equals(ARRAY))
        {
            //if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesArray))
            fuenteDatos = new AlmacenPuntuacionesArray();
        }
        else if(preferenciasPuntaciones.equals(PREFERENCIAS))
        {
            //if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesPreferencias))
            fuenteDatos = new AlmacenPuntuacionesPreferencias(context);
        }
        else if(preferenciasPuntaciones.equals(FICHERO_MEMORIA_INTERNA))
        {
            //if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesFicheroInterno))
            fuenteDatos= new AlmacenPuntuacionesFicheroInterno(context);
        }
        else if(preferenciasPuntaciones.equals(FICHERO_MEMORIA_EXTERNA))
        {
           // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesFicheroExterno))
            fuenteDatos = new AlmacenPuntuacionesFicheroExterno(context);
        }
        else if(preferenciasPuntaciones.equals(FICHERO_MEMORIA_EXTERNA_APLICACION))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesFicheroExternoApl))
            fuenteDatos = new AlmacenPuntuacionesFicheroExternoApl(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_FICHERO_RAW))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesRecursoRaw))
            fuenteDatos = new AlmacenPuntuacionesRecursoRaw(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_FICHERO_ASSETS))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesRecursoRaw))
            fuenteDatos = new AlmacenPuntuacionesRecursoAssets(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_XML_SAX)){

            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesXML_SAX))
            fuenteDatos = new AlmacenPuntuacionesXML_SAX(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_XML_DOM)){

            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesXML_SAX))
            fuenteDatos = new AlmacenPuntuacionesXML_DOM(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_SQLITE))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesSQLite))
            //En el caso de que sea otra instancia borro la base de datos para evitar que haya colision
            /*if(!(fuenteDatos instanceof AlmacenPuntuacionesSQLite))
                context.deleteDatabase(DATABASE_NAME);*/
            fuenteDatos = new AlmacenPuntuacionesSQLite(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_SQLITE_REL))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesSQLiteRel))
            //En el caso de que sea otra instancia borro la base de datos para evitar que haya colision
            /*if(!(fuenteDatos instanceof AlmacenPuntuacionesSQLiteRel))
                context.deleteDatabase(DATABASE_NAME);*/

            fuenteDatos = new AlmacenPuntuacionesSQLiteRel(context);
        }
        else if(preferenciasPuntaciones.equals(LEER_PROVIDER))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesProvider))
            fuenteDatos = new AlmacenPuntuacionesProvider((Activity) context);
        }
        else if(preferenciasPuntaciones.equals(SOCKETS))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesSocket))
            fuenteDatos = new AlmacenPuntuacionesSocket(context);
        }
        else if(preferenciasPuntaciones.equals(SW_PHP))
        {
            // if(fuenteDatos==null || !(fuenteDatos instanceof AlmacenPuntuacionesSocket))
            fuenteDatos = new AlmacenPuntuacionesSW_PHP(context);
        }
        else if(preferenciasPuntaciones.equals(SW_PHP_ASYNCTASK))
        {
            fuenteDatos = new AlmacenPuntuacionesSW_PHP_AsyncTask(context);
        }
        else if(preferenciasPuntaciones.equals(SER_WEB))
        {
            fuenteDatos = new AlmacenPuntuacionesSerWeb();
        }
        else
        {
            //if(fuenteDatos == null)
            fuenteDatos = new AlmacenPuntuacionesArray();
        }

        return fuenteDatos;
    }
}

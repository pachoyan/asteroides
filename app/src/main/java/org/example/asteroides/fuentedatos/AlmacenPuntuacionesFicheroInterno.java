package org.example.asteroides.fuentedatos;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by pauchorroyanguas on 24/10/15.
 */
public class AlmacenPuntuacionesFicheroInterno implements  AlmacenPuntuaciones {

    private static String FICHERO = "puntuaciones.txt";
    private Context context;

    public AlmacenPuntuacionesFicheroInterno(Context context) {
        this.context = context;
    }

    @Override
    public void guardarPuntuacion(int puntos, String nombre, long fecha) {

        final File dir = new File(context.getFilesDir() + "/puntuaciones/");
        if(!dir.exists())
            dir.mkdirs();

        try {

            final File fi = new File(dir, FICHERO);
            FileOutputStream f = new FileOutputStream(fi, true);
            /*FileOutputStream f = context.openFileOutput("puntuaciones/" + FICHERO,
                    Context.MODE_APPEND);*/
            String texto = puntos + " " + nombre + "\n";
            f.write(texto.getBytes());
            f.close();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e); }
    }

    @Override
    public Vector<String> listaPuntuaciones(int cantidad) {

        final File dir = new File(context.getFilesDir() + "/puntuaciones/");
        Vector<String> result = new Vector<String>();
        try {
            final File fi = new File(dir, "puntuaciones.txt");
            FileInputStream f = new FileInputStream(fi);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(f));
            int n = 0;
            String linea;
            do {
                linea = entrada.readLine();
                if (linea != null) { result.add(linea);
                    n++; }
            } while (n < cantidad && linea != null);
            f.close();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e); }
        return result;
    }
}

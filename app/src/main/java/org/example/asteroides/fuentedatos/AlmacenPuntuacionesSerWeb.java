package org.example.asteroides.fuentedatos;

import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by pauchorroyanguas on 29/10/15.
 */
public class AlmacenPuntuacionesSerWeb implements AlmacenPuntuaciones {

    public Vector<String> listaPuntuaciones(int cantidad) {
        HttpURLConnection conexion = null;
        try {
            URL url = new URL("http://158.42.146.127:8080/"
                    + "PuntuacionesServicioWeb/services/PuntuacionesServicioWeb"
                    + "/lista");
            conexion = (HttpURLConnection) url
                    .openConnection();
            conexion.setRequestMethod("POST");
            conexion.setDoOutput(true);
            OutputStreamWriter sal = new OutputStreamWriter(conexion.getOutputStream());
            sal.write("maximo=");
            sal.write(URLEncoder.encode(String.valueOf(cantidad), "UTF-8"));
            sal.flush();
            if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {
                SAXParserFactory fabrica = SAXParserFactory.newInstance();
                SAXParser parser = fabrica.newSAXParser();
                XMLReader lector = parser.getXMLReader();
                ManejadorSerWeb manejadorXML = new ManejadorSerWeb();
                lector.setContentHandler(manejadorXML);
                lector.parse(new InputSource(conexion.getInputStream()));
                conexion.disconnect();
                return manejadorXML.getLista();
            } else {
                Log.e("Asteroides", conexion.getResponseMessage());
                conexion.disconnect();
                return null;
            }
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
            //conexion.disconnect();
            return null;
        }
    }


    public void guardarPuntuacion(int puntos, String nombre, long fecha) {
        try {
            URL url=new URL("http://158.42.146.127:8080/"
                    + "PuntuacionesServicioWeb/services/PuntuacionesServicioWeb"
                    + "/nueva");
            HttpURLConnection conexion = (HttpURLConnection) url
                    .openConnection();
            conexion.setRequestMethod("POST");
            conexion.setDoOutput(true);
            OutputStreamWriter sal = new OutputStreamWriter(
                    conexion.getOutputStream());
            sal.write("puntos=");
            sal.write(URLEncoder.encode(String.valueOf(puntos), "UTF-8"));
            sal.write("&nombre=");
            sal.write(URLEncoder.encode(nombre, "UTF-8"));
            sal.write("&fecha=");
            sal.write(URLEncoder.encode(String.valueOf(fecha), "UTF-8"));
            sal.flush();
            if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {
                SAXParserFactory fabrica = SAXParserFactory.newInstance();
                SAXParser parser = fabrica.newSAXParser();
                XMLReader lector = parser.getXMLReader();
                ManejadorSerWeb manejadorXML = new ManejadorSerWeb();
                lector.setContentHandler(manejadorXML);
                lector.parse(new InputSource(conexion.getInputStream()));
                if (manejadorXML.getLista().size() != 1
                        || !manejadorXML.getLista().get(0).equals("OK")) {
                    Log.e("Asteroides","Error en respuesta servicio Web nueva");
                }
            } else {
                Log.e("Asteroides", conexion.getResponseMessage());
            }
            conexion.disconnect();
        }
        catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
        }
    }


    class ManejadorSerWeb extends DefaultHandler { private Vector<String> lista;

        private StringBuilder cadena;

        public Vector<String> getLista() {
            return lista;
        }
        @Override
        public void startDocument() throws SAXException {
            cadena = new StringBuilder();
            lista = new Vector<String>();
        }
        @Override
        public void characters(char ch[], int comienzo, int longitud){
            cadena.append(ch, comienzo, longitud);
        }
        @Override
        public void endElement(String uri, String nombreLocal, String nombreCualif) throws SAXException {

            if (nombreLocal.equals("return")) {
                try {
                    lista.add(URLDecoder.decode(cadena.toString(), "UTF8"));
                } catch (UnsupportedEncodingException e) {
                    Log.e("Asteroides", e.getMessage(), e);
                }
            }
            cadena.setLength(0);
        }
    }

}



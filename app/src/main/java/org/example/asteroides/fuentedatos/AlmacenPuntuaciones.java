package org.example.asteroides.fuentedatos;

import java.util.Vector;

/**
 * Created by pauchorroyanguas on 9/9/15.
 */
public interface AlmacenPuntuaciones {

    public void guardarPuntuacion(int puntos,String nombre,long fecha);
    public Vector<String> listaPuntuaciones(int cantidad);
}

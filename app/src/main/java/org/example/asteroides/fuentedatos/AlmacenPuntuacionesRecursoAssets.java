package org.example.asteroides.fuentedatos;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.example.asteroides.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by pauchorroyanguas on 24/10/15.
 */
public class AlmacenPuntuacionesRecursoAssets implements  AlmacenPuntuaciones {

    private Context context;

    public AlmacenPuntuacionesRecursoAssets(Context context) {
        this.context = context;
    }

    @Override
    public void guardarPuntuacion(int puntos, String nombre, long fecha) {
        //VACIO
    }

    @Override
    public Vector<String> listaPuntuaciones(int cantidad) {

        Vector<String> result = new Vector<String>();

        String stadoSD = Environment.getExternalStorageState();
        if (!stadoSD.equals(Environment.MEDIA_MOUNTED) &&
                !stadoSD.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            Toast.makeText(context, "No puedo leer en la memoria externa",
                Toast.LENGTH_LONG).show();
            return result;
        }
        
        try {

            //Leemos de assets
            InputStream f = context.getAssets().open("carpeta/puntuaciones.txt");
            BufferedReader entrada = new BufferedReader(new InputStreamReader(f));
            int n = 0;
            String linea;
            do {
                linea = entrada.readLine();
                if (linea != null) { result.add(linea);
                    n++; }
            } while (n < cantidad && linea != null);
            f.close();
        } catch (Exception e) {
            Log.e("Asteroides", e.getMessage(), e);
        }

        return result;
    }
}

# README #

Download the App, and Import from Android Studio as # Open An Existing Android Studio Project #

### What is this repository for? ###

* This repository is for all who want learn Android fundamentals

### What could you find in Asteroides APP? ###

* 0.1 Views And layouts
* 0.2 Activities and Intentions
* 0.3 Drawables and animations
* 0.4 Threads and Sensors
* 0.5 Multimedia and lifecycle activity
* 0.6 Data saving
    * 0.6.1 External Storage SD
    * 0.6.2 External Storage SD Aplication
    * 0.6.3 Preferences
    * 0.6.4 Internal Storage
    * 0.6.5 XML with SAX
    * 0.6.6 XML with DOM
    * 0.6.7 Data base SQLITE
    * 0.6.8 Resource files 1: Assets
    * 0.6.9 Resource files 2: Raw
    * 0.6.10 Content Provider 
* 0.7 WWW, Sockets, HTTP and Web Services
   *  0.7.1 Socket
   *  0.7.2 WS Apache PHP and MySQL
   *  0.7.3 WS Apache PHP and MySQL with AsyncTask
    * 0.7.4. Web Service with Axis 2


### Author ###

Pau Chorro Yanguas, Spain